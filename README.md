## Docpad Catalogs Plugin

This plugin acts as a crawler that scans a physical folder structure to identify catalogs/products and generates configured pages for them.

## Overview

It expects the following structure in your `./src/files` folder:

* src
    * files
        * catalogs
            * catalog1
                * product1
                    * thumbnail
                        * thumb.jpg
                    * 1.jpg
                    * 2.jpg
                * product1.yaml
            * catalog1.yaml

Where

* a catalog (or product) has one asset folder and one metadata file (could be .yaml, .yml or .json)
* a catalog may have zero to many products (contained in the catalog's asset folder)
* a product may have zero to many images (contained in the product's asset folder)
* a product may have zero or one thumbnail image (contained in the thumbnail sub-folder)
    * if no thumbnail image is found, the first product image will be used
* the minimal properties in each metadata file are
    * id
    * name
    * folderName

Please refer to the `example` folder in this repo for more details of metadata and folders.

## Configurations

You may configure the plugin in your `docpad.coffee`:

```coffeescript
plugin:
    catalogs:

        # must be relative to ./out/
        # defaults to "catalogs"
        path: "properties"

        # available through docpad.getCollection()
        collectionNames:

            # defaults to "catalogs"
            catalogs: "areas",

            # defaults to "products"
            products: "properties"

        # the documents to be generated
        # to disable a page generation, just set it as false, ie. catalogs: false
        pages:

            # only one will be generated
            catalogs: 

                # defaults to "Catalogs"
                title: "All Properties"

                # define Docpad layout to use
                # defaults to "default"
                layout: "property-list"

            # one generated for each
            catalog:

                # defaults to "default"
                layout: "area-list"

            # one generated for each
            product:

                # defaults to "default"
                layout: "property"

        # add more to these defaults as necessary
        imageFileExtensions: ["png", "PNG", "jpg", "JPG", "jpeg", "JPEG"]
```