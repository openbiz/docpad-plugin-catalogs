var multiparty = require("multiparty");
var path = require("path");
var slash = require("slash");
var Q = require("q");
var slug = require("slug");
var yaml = require("yamljs");
var queryEngine = require("query-engine");

var BackboneModel = queryEngine.Backbone.Model;

module.exports = function(BasePlugin) {

	return BasePlugin.extend({

		name: "catalogs",

		config: {

			// must be relative to ./out/
			path: "catalogs",

			collectionNames: {
				catalogs: "catalogs",
				products: "products"
			},

			/* Defines metadata to use to generate pages.
			 * These pages can access custom collections ("catalogs" and "products") through getCollection function.
			 * 
			 * WARN: do not define any metadata in the actual layout files other than parent "layout"
			 */
			pages: {
				// one page
				catalogs: {
					title: "Catalogs",
					layout: "default"
				},
				// one page for each
				// document metadata contains catalog and catalogId
				catalog: {
					layout: "default"
				},
				// one page for each
				// document metadata contains catalog, product and productId
				product: {
					layout: "default"
				}
			},

			pageFileName: "index.html",

			metadataFileParsers: {
				"yaml": function parseYaml(content) {
					return yaml.parse(content);
				},
				"yml": function parseYaml(content) {
					return yaml.parse(content);
				},
				"json": function parseJson(content) {
					return JSON.parse(content);
				}
			},

			imageFileExtensions: ["png", "PNG", "jpg", "JPG", "jpeg", "JPEG"]
		},

		/*
		 * Docpad events
		 * ================================
		 */

		populateCollections: function populateCollections(opts, next) {
			var self = this;

			self._generateCatalogs(next);
		},

		generateBefore: function generateBefore(opts, next) {
			var self = this;

			if (opts.reset) {
				// a full-regeneration, the "populateCollections" should have handled it
				next();
			} else {
				// it's a partial-regeneration, handle it
				// TODO: this is quite expensive.. find better way
				self._generateCatalogs(next);
			}
		},

		/*
		 * Plugin functions
		 * ================================
		 */

		// NOTE: wrap caller with try..catch
		_parseMetadata: function _parseMetadata(metadataFile) {
			var self = this;
			var config = self.config;
			var metadata;
			var fileExt = metadataFile.extension;
			var parser = config.metadataFileParsers[fileExt];

			if (!parser) {
				throw "Invalid metadata file format: " + metadataFile.filename + ". It must be one of " + Object.keys(config.metadataFileParsers).join(", ");
			}

			metadata = parser(metadataFile.source);

			/*if (metadata.disable) {
				
				 * NOTE:
				 * when the system is running, deleting/renaming the files will trigger error
				 * so we're supporting disabling a catalog or product instead.
				 * 
				 * https://github.com/bevry/docpad/issues/792
				 
				throw "Found disabled metadata file: " + metadataFile.filename + ". The system will skip this entry.";
			}*/

			return metadata;
		},

		_createCollection: function _createCollection(name, data) {
			// NOTE: we're using the barebone model instead of Docpad's FileModel
			var collection = queryEngine.createLiveCollection(data, {
				model: BackboneModel
			});

			this.docpad.setCollection(name, collection);
		},

		_getFilesAtPath: function _getFilesAtPath(path, extensions, sorting, paging) {
			// use docpad.getFiles() with exact parent folder path instead of $startsWith (used by getFilesAtPath)
			// - we support only relative path to our use case
			// - and allow specification of extensions
			var query = {
				relativeDirPath: path
			};

			if (extensions && extensions.length != 0) {
				if (!(extensions instanceof Array)) {
					extensions = [extensions];
				}

				query.extension = {
					$in: extensions
				};
			}

			return this.docpad.getFiles(query, sorting, paging);
		},

		_guessFolderName: function _guessFolderName(metadata) {
			return slug(metadata.id);
		},

		_generateCatalogs: function _generateCatalogs(next) {
			var self = this;
			var docpad = self.docpad;
			var config = self.config;

			// generate page first, because it might be needed even when there's no data
			if (config.pages.catalogs) {
				var pagePath = path.join(config.path, config.pageFileName);

				// don't pollute the config
				var metaData = JSON.parse(JSON.stringify(config.pages.catalogs));
				// inject
				metaData.relativePath = pagePath;
				// need this to allow updating of document instead of creating
				metaData.isCatalogsPage = true;

				self._upsertDocument({
					searchQuery: {
						isCatalogsPage: true
					},
					metaData: metaData
				});
			}

			var catalogMetadataFiles = self._getFilesAtPath(config.path, Object.keys(config.metadataFileParsers)).toJSON();

			if (!catalogMetadataFiles || catalogMetadataFiles.length == 0) {
				docpad.log("info", "No catalog metadata file is found");
				return next();
			}

			var promises = [];

			catalogMetadataFiles.forEach(function(catalogMetadataFile) {
				var catalogMetadata;

				try {
					catalogMetadata = self._parseMetadata(catalogMetadataFile);

				} catch (ex) {
					docpad.log("warn", "Failed on " + catalogMetadataFile.filename + ": " + JSON.stringify(ex));

					// just don't promise anything
					return;
				}

				// expect a promise to return
				var promise = self._generateCatalog(catalogMetadata);

				promises.push(promise);
			});

			Q.all(promises).then(
				function success(catalogs) {
					// populate "catalogs" collection
					self._createCollection(config.collectionNames.catalogs, catalogs);

					var allProducts = [];

					catalogs.forEach(function(catalog) {
						allProducts = allProducts.concat(catalog.products);
					});

					// populate "products" collection
					self._createCollection(config.collectionNames.products, allProducts);

					docpad.log("info", "Catalogs and products collections are generated successfully");
				}, 
				function error(reason) {
					docpad.log("warn", "Catalogs generation has failed: " + reason);
				}
			)["finally"](
				function() {
					// proceed anyway
					next();
				}
			);
		},

		_generateCatalog: function _generateCatalog(catalogMetadata) {
			var self = this;
			var config = self.config;

			if (!catalogMetadata.folderName) {
				catalogMetadata.folderName = self._guessFolderName(catalogMetadata);
			}

			// generate page first, because it might be needed even when there's no data
			if (config.pages.catalog) {
				var pagePath = path.join(config.path, catalogMetadata.folderName, config.pageFileName);
				// don't pollute the config
				var metaData = JSON.parse(JSON.stringify(config.pages.catalog));
				// inject
				metaData.relativePath = pagePath;
				metaData.relativePath = pagePath;
				metaData.title = metaData.title || catalogMetadata.name;
				metaData.catalog = catalogMetadata;
				// need this to allow updating of document instead of creating
				metaData.catalogId = catalogMetadata.id;

				self._upsertDocument({
					searchQuery: {
						catalogId: catalogMetadata.id
					},
					metaData: metaData
				});
			}

			return self._generateProducts(catalogMetadata).then(
				function success(products) {
					// insert
					catalogMetadata.products = products;

					return catalogMetadata;
				}
			);
		},

		_generateProducts: function _generateProducts(catalogMetadata) {
			var self = this;
			var docpad = self.docpad;
			var config = self.config;

			var catalogId = catalogMetadata.id;
			var catalogName = catalogMetadata.name;
			var catalogFolderName = catalogMetadata.folderName;

			var folderPath = path.join(config.path, catalogFolderName);
			var productMetadataFiles = self._getFilesAtPath(folderPath, Object.keys(config.metadataFileParsers)).toJSON();

			if (!productMetadataFiles || productMetadataFiles.length == 0) {
				docpad.log("info", "No product metadata file is found for: " + catalogName);
				// so the catalog should have no products
				// let's not waste time
				var deferred = Q.defer();
				deferred.resolve([]);

				return deferred.promise;
			}

			var promises = [];

			productMetadataFiles.forEach(function(productMetadataFile) {
				var productMetadata;

				try {
					productMetadata = self._parseMetadata(productMetadataFile);

				} catch (ex) {
					docpad.log("warn", "Failed on " + productMetadataFile.filename + ": " + JSON.stringify(ex));

					// just don't promise anything
					return;
				}

				var promise = self._generateProduct(catalogMetadata, productMetadata);

				promises.push(promise);
			});

			return Q.all(promises);
		},

		_generateProduct: function _generateProduct(catalogMetadata, productMetadata) {
			var self = this;
			var docpad = self.docpad;
			var config = self.config;

			var deferred = Q.defer();
			var promise = deferred.promise;

			if (!productMetadata.folderName) {
				productMetadata.folderName = self._guessFolderName(productMetadata);
			}

			// NOTE: we have a strict folder structure
			var productFolderPath = path.join(config.path, catalogMetadata.folderName, productMetadata.folderName);

			// generate page first, because it might be needed even when there's no data
			if (config.pages.product) {
				var pagePath = path.join(productFolderPath, config.pageFileName);
				
				// don't pollute the config
				var metaData = JSON.parse(JSON.stringify(config.pages.product));

				// inject
				metaData.relativePath = pagePath;
				metaData.title = metaData.title || productMetadata.name;
				metaData.catalog = catalogMetadata;
				metaData.product = productMetadata;
				// need this to allow updating of document instead of creating
				metaData.productId = productMetadata.id;

				self._upsertDocument({
					searchQuery: {
						productId: productMetadata.id
					},
					metaData: metaData
				});
			}

			var productImageFiles = self._getFilesAtPath(productFolderPath, config.imageFileExtensions).toJSON();
			// grab the first (if any)
			var thumbnailFolderPath = path.join(productFolderPath, "thumbnail");
			var productThumbnailFile = self._getFilesAtPath(thumbnailFolderPath, config.imageFileExtensions).toJSON()[0];

			// NOTE: these are of Docpad's FileModel
			productMetadata.imageFiles = productImageFiles;
			productMetadata.thumbnailFile = productThumbnailFile || productImageFiles[0];

			deferred.resolve(productMetadata);

			return promise;
		},

		// WARN: metadata set in actual document/layout will overwrite what's set here
	    _upsertDocument:  function _upsertDocument(opts) {
	        var self = this;
	        var docpad = self.docpad;
	        var searchQuery = opts.searchQuery;
	        var content = opts.content;
	        var metaData = opts.metaData;
	        var doc = searchQuery ? docpad.getFile(searchQuery) : null;
	        var docAttrs = {
	            data: content,
	            meta: metaData
	        };

	        if (doc != null) {
	            doc.set(docAttrs);
	        } else {
	            doc = docpad.createDocument(docAttrs);
	        }

	        // adapted from the tumblr plugin
	        doc.action("load", function(error) {
	            if (error) {
	                return console.error("Failed in loading dynamic document: " + error);
	            }

	            if (typeof docpad.addModel === "function") {
	                docpad.addModel(doc);
	            } else {
	                docpad.getDatabase().add(doc);
	            }
	        });

	        return doc;
	    }

	});

};